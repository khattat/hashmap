#ifndef WORDCOUNT_H
#define WORDCOUNT_H

#include <stdio.h>
#include "hashmap.h"

void count_words(FILE *stream);

#endif /* ifndef WORDCOUNT_H */
