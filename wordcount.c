#include "wordcount.h"
#include <ctype.h>

#define BUFSIZE 128
#define STRFORMAT "%128s"

static void *resolve_collision(void *old_data, void *new_data);
static void destroy_data(void *data) { free(data); }
static void count_and_add_words(HashMap *hm, const char *buf);
static HashMap *read_file(FILE *stream);
static void print_counts(HashMap *hm);
static void print_element(const char *key, void *data);

void count_words(FILE *stream) {
  HashMap *hm = read_file(stream);
  if (!hm) return;
  print_counts(hm);
  delete_hashmap(hm, destroy_data);
}

HashMap *read_file(FILE *stream) {
  HashMap *hm = create_hashmap(1024);
  if (!hm) return NULL;
  char buf[BUFSIZE] = { 0 };
  while (fscanf(stream, STRFORMAT, buf) != EOF) {
    count_and_add_words(hm, buf);
  }
  return hm;
}

void count_and_add_words(HashMap *hm, const char *buf) {
  int c = 0;
  char alnum[BUFSIZE + 1] = { 0 };
  do {
    if (isalnum(*buf)) {
      alnum[c] = *buf;
      c++;
    } else {
      if (!c) continue;
      alnum[c] = '\0';
      int *d = malloc(sizeof *d);
      *d = 1;
      insert_data(hm, alnum, d, resolve_collision);
      c = 0;
    }
  } while (*buf++);
}

static void *resolve_collision(void *old_data, void *new_data) {
  int *o = (int *)old_data;
  int *n = (int *)new_data;
  *o = *o + *n;
  free(new_data);
  return old_data;
}

void print_counts(HashMap *hm) { iterate(hm, print_element); }

void print_element(const char *key, void *data) {
  int *d = (int *)data;
  printf("%s: %d\n", key, *d);
}
