#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hashmap.h"

#ifdef COUNTING_WORDS
#include "wordcount.h"
#endif

#define NELEMS 10000
unsigned int counter;

size_t mystrlen(const char *str) { return strlen(str) + 1; }

void print_element(const char *key, void *data) {
  printf("(\"%s\", %s)\n", key, (char *)data);
}

void check_keys_once(const char *key, void *data) {
  static int visited[NELEMS] = {0};
  (void)data;
  int key_index;
  sscanf(key, "key_%d", &key_index);
  assert(visited[key_index] == 0);  // got the key more than once if it's 1
  visited[key_index] = 1;
}

void check_data(const char *key, void *data) {
  int key_index;
  int data_index;
  sscanf(key, "key_%d", &key_index);
  sscanf(data, "data_%d", &data_index);
  assert(key_index == data_index);
}

void empty_iterate(const char *key, void *data) {
  (void)key;
  (void)data;
  assert(0);
}

void check_size(const char *key, void *data) {
  // consume key and data
  (void)key;
  (void)data;
  counter++;
}

void remove_data_check_size(void *data) {
  // consume key and data
  (void)data;
  counter++;
}

void *resolve_collision(void *old_value, void *new_value) {
  (void)old_value;  // use old_value
  return new_value;
}

unsigned int my_prehash(const char *key) {
  size_t len = strlen(key);
  unsigned int prehash = 0;
  for (size_t n = 0; n < len; n++) {
    prehash = 31 * prehash + key[n];
  }
  return prehash;
}

void noob_test(HashMap *hm) {
  char *string_1 = "TI2725-C";
  char *string_2 = "Embedded";
  char *string_3 = "Software";
  char *string_5 = "TOM";
  char *string_6 = "JERRY";
  const char *key_1 = "ab";
  const char *key_2 = "cd";
  const char *key_3 = "ad";
  const char *key_4 = "xy";
  const char *key_5 = "B1";
  const char *key_6 = "A2";

  // Insert ("ab" -> "TI2725-C").
  insert_data(hm, key_1, string_1, resolve_collision);
  assert(memcmp(get_data(hm, key_1), string_1, mystrlen(string_1)) == 0);

  // Insert ("cd" -> "Embedded").
  insert_data(hm, key_2, string_2, resolve_collision);
  assert(memcmp(get_data(hm, key_2), string_2, mystrlen(string_2)) == 0);

  // Insert ("ad" -> "Software").
  insert_data(hm, key_3, string_3, resolve_collision);
  assert(memcmp(get_data(hm, key_3), string_3, mystrlen(string_3)) == 0);

  // Insert ("ab" -> "Embedded").
  insert_data(hm, key_1, string_2, resolve_collision);
  assert(memcmp(get_data(hm, key_1), string_2, mystrlen(string_2)) == 0);

  // Insert ("B1" -> "Mostafa") HASH ID: 115
  insert_data(hm, key_5, string_5, resolve_collision);
  assert(memcmp(get_data(hm, key_5), string_5, mystrlen(string_5)) == 0);

  // Insert ("A2" -> "Khattat") HASH ID: 115
  insert_data(hm, key_6, string_6, resolve_collision);
  assert(memcmp(get_data(hm, key_6), string_6, mystrlen(string_6)) == 0);

  // Insert ("ad" -> NULL)
  insert_data(hm, key_3, NULL, resolve_collision);
  assert(get_data(hm, key_3) == NULL);

  // Get data for a not inserted key.
  assert(get_data(hm, "C0") == NULL);
  assert(get_data(hm, key_4) == NULL);

  // Iterate the hash map
  iterate(hm, print_element);

#ifdef NEW_HASH
  set_prehash_function(hm, my_prehash);

  printf("\nHERE WE GO AGAIN!\n\n");

  // Iterate the hash map
  iterate(hm, print_element);
#endif

  // Delete key "cd".
  remove_data(hm, key_2, NULL);
  assert(get_data(hm, key_2) == NULL);

  // Delete key "ab".
  remove_data(hm, key_1, NULL);
  assert(get_data(hm, key_1) == NULL);

  // Delete key "B1".
  remove_data(hm, key_5, NULL);
  assert(get_data(hm, key_5) == NULL);

#ifdef COUNTING_WORDS
  printf("\nHERE WE GO COUNTING!\n\n");
  // Create a temporary file
  FILE *stream = tmpfile();

  // Write to the stream
  fprintf(stream, "foo bar_, baz!\n");
  fprintf(stream, "foo\t\"bar\". mostafa_khattat mostafa bahal ast\n");
  fprintf(stream, "foo?\n");

  // Set the position to the start of the stream
  fseek(stream, 0, SEEK_SET);

  // Count the words
  count_words(stream);

  // Close the file
  fclose(stream);
#endif
}

void check_remove_none_existing_key(HashMap *hm) {
  remove_data(hm, "BATMAN", NULL);
}

void check_iterate_keys_once(HashMap *hm) { iterate(hm, check_keys_once); }

void check_data_iteration_test(HashMap *hm) { iterate(hm, check_data); }

void check_data_using_get(HashMap *hm, size_t size) {
  for (size_t i = 0; i < size; i++) {
    char key[15];
    char data[15];
    sprintf(key, "key_%zu", i);
    sprintf(data, "data_%zu", i);
    assert(memcmp(get_data(hm, key), data, mystrlen(data)) == 0);
  }
}

void check_empty_iterate(HashMap *hm) { iterate(hm, empty_iterate); }

void check_hm_size(HashMap *hm, unsigned int expected_size) {
  counter = 0;
  iterate(hm, check_size);
  assert(counter == expected_size);
}

void check_remove_data(HashMap *hm, unsigned int hm_size) {
  // remove half of the data
  for (unsigned int i = 0; i < hm_size; i += 2) {
    char key[15];
    sprintf(key, "key_%u", i);
    remove_data(hm, key, NULL);
    assert(get_data(hm, key) == NULL);
  }
  // check size of the hm
  check_hm_size(hm, hm_size / 2);

  assert(hm->size == NELEMS / 2);
  printf("new key space after deleting 5000 elems is %zu and size is %zu\n", hm->m, hm->size);

  // we should be able to access the other half
  for (unsigned int i = 1; i < hm_size; i += 2) {
    char key[15];
    char data[15];
    sprintf(key, "key_%u", i);
    sprintf(data, "data_%u", i);
    assert(memcmp(get_data(hm, key), data, mystrlen(data)) == 0);
  }


  for (unsigned int i = 1; i < hm_size / 2; i += 2) {
    char key[15];
    sprintf(key, "key_%u", i);
    remove_data(hm, key, NULL);
    assert(get_data(hm, key) == NULL);
  }
  assert(hm->size == NELEMS / 4);
  printf("new key space after deleting another 2500 elems is %zu and size is %zu\n", hm->m, hm->size);

  // remove the other half and check the size
  counter = 0;
  delete_hashmap(hm, remove_data_check_size);
  assert(counter == hm_size / 4);
}

void remove_all_and_insert(HashMap* hm, unsigned int hm_size) {
  printf("new key space before deleting all data is %zu and size is %zu\n", hm->m, hm->size);
  for (unsigned int i = 0; i < hm_size; i++) {
    char key[15];
    sprintf(key, "key_%u", i);
    remove_data(hm, key, NULL);
    assert(get_data(hm, key) == NULL);
    /* printf("new key space is %zu and size is %zu\n", hm->m, hm->size); */
  }

  printf("new key space after deleting all data is %zu and size is %zu\n", hm->m, hm->size);

  printf("Insert Data after removing everything:\n");

  char *string_1 = "TI2725-C";
  const char *key_1 = "ab";
  // Insert ("ab" -> "TI2725-C").
  insert_data(hm, key_1, string_1, resolve_collision);
  printf("new key space after inserting 1 data is %zu and size is %zu\n", hm->m, hm->size);
  remove_data(hm, key_1, NULL);
  printf("new key space after deleting 1 data is %zu and size is %zu\n", hm->m, hm->size);
  insert_data(hm, key_1, string_1, resolve_collision);
  printf("new key space after inserting 1 data is %zu and size is %zu\n", hm->m, hm->size);
  remove_data(hm, key_1, NULL);
  printf("new key space after deleting 1 data is %zu and size is %zu\n", hm->m, hm->size);
  insert_data(hm, key_1, string_1, resolve_collision);
  printf("new key space after inserting 1 data is %zu and size is %zu\n", hm->m, hm->size);
  delete_hashmap(hm, NULL);
}

void check_count_words(const char *path) {
  FILE *stream = fopen(path, "r");
  if (!stream) return;
  fseek(stream, 0, SEEK_SET);
  #ifdef COUNTING_WORDS
    count_words(stream);
  #endif
    fclose(stream);
}

int main() {
  // create an empty hashmap hm
  HashMap *hm1 = create_hashmap(1024);

  // some happy tests
  noob_test(hm1);

  check_remove_none_existing_key(hm1);

  delete_hashmap(hm1, NULL);

  // create an empty hashmap hm2
  HashMap *hm2 = create_hashmap(1024);
  printf("key space after creating an empty hashmap is %zu and size is %zu\n", hm2->m, hm2->size);
  assert(hm2->size == 0);

  // there should be no call to iteration call back function
  check_empty_iterate(hm2);

  // initialize hm2 with NELEMS keys and data
  // each key has an index starting from zero and each corresponding data
  // will have the same index.
  char data[NELEMS][15];
  char key[NELEMS][15];
  for (unsigned int i = 0; i < NELEMS; i++) {
    sprintf(key[i], "key_%u", i);
    sprintf(data[i], "data_%u", i);
    insert_data(hm2, key[i], data[i], resolve_collision);
    assert(memcmp(get_data(hm2, key[i]), data[i], mystrlen(data[i])) == 0);
  }

  printf("key space after inserting %d elems is %zu and size is %zu\n", NELEMS, hm2->m, hm2->size);
  assert(hm2->size == NELEMS);

#ifdef NEW_HASH
  // when changing the prehash function we should still be able to
  // match keys and data
  set_prehash_function(hm2, my_prehash);
#endif

  // when iterating over items keys should have their corresponding data
  check_data_iteration_test(hm2);

  // the same as check_data_iteration_test except instead of using iterate, we will use get_data
  check_data_using_get(hm2, NELEMS);

  // use prev. initialized values to see if we iterate over keys only once
  check_iterate_keys_once(hm2);

  // hm2 should have the size of NELEMS
  check_hm_size(hm2, NELEMS);

  // check remove_data and delete_hashmap
  // also DELETE hm2
  check_remove_data(hm2, NELEMS);

  /* check_count_words("Clarissa.txt"); */

  // create an empty hashmap hm3
  HashMap *hm3 = create_hashmap(1024);

  // initialize hm3 with NELEMS keys and data
  // each key has an index starting from zero and each corresponding data
  // will have the same index.
  for (unsigned int i = 0; i < NELEMS; i++) {
    sprintf(key[i], "key_%u", i);
    sprintf(data[i], "data_%u", i);
    insert_data(hm3, key[i], data[i], resolve_collision);
    assert(memcmp(get_data(hm3, key[i]), data[i], mystrlen(data[i])) == 0);
  }
  remove_all_and_insert(hm3, NELEMS);

  return 0;
}
