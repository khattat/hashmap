#ifndef HASHMAP_H
#define HASHMAP_H

#include <stdlib.h>

#define NEW_HASH

typedef struct Pair Pair;

struct Pair {
  Pair *next;
  char *key;
  void *value;
};

typedef struct {
  /* I'm sure you won't listen but, please don't touch these members */
  size_t m; // keyspace
  size_t size;
  unsigned int (*prehash)(const char *key);
  Pair **buckets;
} HashMap;

HashMap *create_hashmap(size_t key_space);
void insert_data(HashMap *hm, const char *key, void *data,
                 void *(*resolve_collision)(void *old_data, void *new_data));
void *get_data(HashMap *hm, const char *key);
void iterate(HashMap *hm, void (*callback)(const char *key, void *data));
void remove_data(HashMap *hm, const char *key,
                 void (*destroy_data)(void *data));
void delete_hashmap(HashMap *hm, void (*destroy_data)(void *data));
void set_prehash_function(HashMap *hm, unsigned int (*new_prehash)(const char *key));

#endif /* ifndef HASHMAP_H */
