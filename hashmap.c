#include "hashmap.h"
#include <stdio.h>
#include <string.h>

static int add_to_linkedlist(Pair *start, Pair *item,
                             void *(*resolve_collision)(void *old_data,
                                                        void *new_data));
static Pair *create_pair(const char *key, void *data);
static void free_pair(Pair *pair);
static unsigned int prehash(const char *key);
static void rehash(HashMap *hm);
static HashMap *create_hashmap_wrapper(
    size_t key_space, unsigned int (*prehash_default)(const char *key));
static void resize(HashMap *hm, size_t new_keyspace);

static HashMap *create_hashmap_wrapper(
    size_t key_space, unsigned int (*default_prehash)(const char *key)) {
  if (key_space < 1) return NULL;
  HashMap *hm = malloc(sizeof *hm);
  if (!hm) return NULL;

  hm->prehash = default_prehash;
  hm->size = 0;
  hm->m = key_space;
  hm->buckets = calloc(key_space, sizeof *hm->buckets);
  if (!hm->buckets) return NULL;
  return hm;
}

HashMap *create_hashmap(size_t key_space) {
  return create_hashmap_wrapper(key_space, prehash);
}

unsigned int prehash(const char *key) {
  unsigned int ph = 0;
  size_t len = strlen(key);
  for (size_t i = 0; i < len; i++) {
    ph += key[i];
  }
  return ph;
}

void insert_data(HashMap *hm, const char *key, void *data,
                 void *(*resolve_collision)(void *old_data, void *new_data)) {
  if (!key || !hm) return;
  Pair *new_pair = create_pair(key, data);
  if (!new_pair) return;

  unsigned int hid = hm->prehash(key) % hm->m;
  if (hm->buckets[hid]) { /* if there are already pairs in this bucket */
    if (add_to_linkedlist(hm->buckets[hid], new_pair, resolve_collision))
      hm->size += 1;
  } else { /* if this bucket is empty */
    hm->buckets[hid] = new_pair;
    hm->size += 1;
  }

  /* Table Doubling: if the size of items in the table
   * are greater than key space then it's time to grow
   * the table by 2 times the original key space size
   */
  if (hm->size > hm->m) resize(hm, 2 * hm->m);
}

Pair *create_pair(const char *key, void *data) {
  if (!key) return NULL;
  size_t key_len = strlen(key) + 1;

  Pair *new_pair = malloc(sizeof *new_pair);
  if (!new_pair) return NULL;

  new_pair->key = malloc(key_len);
  if (!new_pair->key) return NULL;

  new_pair->next = NULL;
  memcpy(new_pair->key, key, key_len);
  new_pair->value = data;

  return new_pair;
}

/*
 * If there is a collision and we didn't add anything new
 * except calling resolve_collision and updating the linkedlist return 0
 * otherwise 1
 */
int add_to_linkedlist(Pair *start, Pair *item,
                      void *(*resolve_collision)(void *old_data,
                                                 void *new_data)) {
  size_t len = strlen(item->key) + 1;
  Pair *cpair = start;
  Pair *ppair = start;

  /* check for any collision */
  while (cpair) {
    if (memcmp(cpair->key, item->key, len) == 0) {
      cpair->value = resolve_collision(cpair->value, item->value);
      free_pair(item);
      return 0;
    }
    ppair = cpair;
    cpair = cpair->next;
  }

  /* there is no collision at this point so add the item to the end of the
   * linklist */
  ppair->next = item;
  return 1;
}

static void resize(HashMap *hm, size_t new_keyspace) {
  Pair **new_buckets = calloc(new_keyspace, sizeof *new_buckets);
  if (!new_buckets) return;

  /* for each item in the hashmap recalculate the hash id
   * and put the item inside the new buckt list
   */
  for (size_t i = 0; i < hm->m; i++) {
    Pair *opair = hm->buckets[i];
    while (opair) {
      Pair *next = opair->next;
      unsigned int hid = hm->prehash(opair->key) % new_keyspace;
      Pair *tmp = new_buckets[hid];
      new_buckets[hid] = opair;
      opair->next = tmp;
      opair = next;
    }
  }

  free(hm->buckets);
  hm->buckets = new_buckets;
  if (new_keyspace < 1)
    hm->m = 1;
  else
    hm->m = new_keyspace;
}

void *get_data(HashMap *hm, const char *key) {
  if (!key || !hm) return NULL;
  unsigned int hid = hm->prehash(key) % hm->m;
  if (!hm->buckets[hid]) return NULL;

  size_t len = strlen(key) + 1;
  Pair *n = hm->buckets[hid];
  while (n) {
    if (memcmp(n->key, key, len) == 0) return n->value;
    n = n->next;
  }

  return NULL;  // Nothing has been found
}

void iterate(HashMap *hm, void (*callback)(const char *key, void *data)) {
  if (!hm) return;
  for (size_t i = 0; i < hm->m; i++) {
    if (!hm->buckets[i]) continue;
    Pair *n = (Pair *)hm->buckets[i];
    while (n) {
      if (callback != NULL) callback(n->key, n->value);
      n = n->next;
    }
  }
}

void remove_data(HashMap *hm, const char *key,
                 void (*destroy_data)(void *data)) {
  if (!key || !hm) return;
  unsigned int hid = hm->prehash(key) % hm->m;
  if (!hm->buckets[hid]) return;

  size_t len = strlen(key) + 1;
  Pair *cpair = hm->buckets[hid];
  Pair **ppair = &hm->buckets[hid];

  /* find the key in the linkedlist */
  while (cpair) {
    if (memcmp(cpair->key, key, len) == 0) {
      *ppair = cpair->next;
      if (destroy_data) destroy_data(cpair->value);
      free_pair(cpair);
      hm->size -= 1;
      break;
    }
    ppair = &(cpair->next);
    cpair = cpair->next;
  }

  /* Table Doubling: if the size of items in the table
   * are less than a quarter of key space then it's time to shrink
   * the table by 2 times the original key space size
   */
  if (hm->size > 0 && hm->m > (4 * hm->size)) resize(hm, (hm->m / 2));
}

void free_pair(Pair *pair) {
  free(pair->key);
  free(pair);
}

void delete_hashmap(HashMap *hm, void (*destroy_data)(void *data)) {
  if (!hm) return;
  for (size_t i = 0; i < hm->m; i++) {
    Pair *cpair = hm->buckets[i];
    while (cpair) {
      Pair *tmp = cpair;
      cpair = cpair->next;
      if (destroy_data) destroy_data(tmp->value);
      free_pair(tmp);
      hm->size -= 1;
    }
  }
  free(hm->buckets);
  free(hm);
}

void set_prehash_function(HashMap *hm,
                       unsigned int (*new_prehash)(const char *key)) {
  if (hm && new_prehash) {
    hm->prehash = new_prehash;
    rehash(hm);
  }
}

void rehash(HashMap *hm) { resize(hm, hm->m); }

